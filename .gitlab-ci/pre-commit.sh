#!/bin/bash

set -e

python3 -m venv "${VENV}"
export PATH="${VENV}"/bin:"${PATH}"
pip3 install -r requirements.txt
pip install \
    pre-commit \
    pylint
git fetch origin
pre-commit run --from-ref origin/"${CI_DEFAULT_BRANCH}" --to-ref HEAD
