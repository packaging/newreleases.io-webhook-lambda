[[_TOC_]]

# newreleases.io webhook for AWS Lambda

## Installation process overview

1. create account on [](https://newreleases.io)
1. [configure webhook on Newreleases](#configure-webhook)
1. [create IAM polcies](#policies)
1. [prepare AWS Lambda code](#prepare-function-code)
1. [create AWS Lambda Function](#create-function)
1. update Newreleases webhook and set copied `FunctionUrl` as URL
1. add projects to be watched by Newreleases
1. [add note to projects](#addconfigure-project) that should be handled by this system

## Newreleases.io

### Configure webhook

* Add Header
   * `x-gitlab-private-token` with your private gitlab token to create tags (role: Developer, permission: write_repository)
* activate `Custom JSON Request Template` and enter the following
   ```json
   {"project":"{project}","version":"{version}","note":{"message":"{note_message}"},"project_note":"{project_note}"}
   ```
* set some random valid URL, e.g. `https://invalid.domain/invalid`
* copy generated newreleases webhook key to be used for `NEWRELEASES_WEBHOOK_KEY`

### Add/configure project

For every project to be managed via this webhook add config to project note:

```json
{
  "gitlab": {
    "project": <gitlab-project-id>,
    "branch": "main",
    "trigger": <pipeline-trigger-token>
  }
}
```

* `project` specifies the numeric ID you can find on the start page of your Gitlab project
* `branch` specifies the branch to tag in your Gitlab project (optional, default = `master`)
* `trigger` (_optional_): specifies a pipeline trigger token if you want to trigger a pipeline for the specific tag (e.g. use for delayed pipelines w/ `when: delayed` + `start_in: <time>` + `only: [triggers]`)

## IAM

### Policies

#### `CloudWatchLogsAccess`

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "logs:CreateLogGroup",
            "Resource": "arn:aws:logs:<REGION>:<ACCOUNT-ID>:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": [
                "arn:aws:logs:<REGION>:<ACCOUNT-ID>:log-group:/aws/lambda/*:*"
            ]
        }
    ]
}
```

#### `AmazonSNSPublishAccess`

Optional. If you want to receive notifications from the hook.

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "sns:Publish",
            "Resource": "arn:aws:sns:*:<ACCOUNT-ID>:*"
        }
    ]
}
```

### Roles

* create `AWSLambdaBasicExecutionRole`
* attach `CloudWatchLogsAccess` and `AmazonSNSPublishAccess` policies

## Notifications (optional)

If you like to receive emails about actions, just use an existing SNS topic or create a new one e.g. using the e-mail protocol.
When using e-mail via SNS, just make sure to use or create a verified identity in SNS.

## Lambda

### Prepare Function Code

```bash
DOCKER_DEFAULT_PLATFORM=linux/arm64 docker run --rm \
  -v "${PWD}:/build" \
  -w /build \
  python:3.9-slim \
  sh -c 'pip3 install -r requirements.txt --target package; chown -R "$(stat -c %u /build):$(stat -c %g /build)" .'
cd package
zip -r9 ../lambda_function.zip .
cd -
zip -g lambda_function.zip lambda_function.py
```

### Create Function

```bash
export REGION="...." AWSLAMBDABASICEXECUTIONROLE_ARN="arn:aws:iam::....:role/...." NEWRELEASES_WEBHOOK_KEY="...."
# prepare function
aws \
  lambda \
  create-function \
  --region "${REGION}" \
  --function-name newreleases-webhook-lambda \
  --runtime python3.9 \
  --role "${AWSLAMBDABASICEXECUTIONROLE_ARN}" \
  --handler lambda_function.lambda_handler \
  --architectures arm64 \
  --environment Variables={NEWRELEASES_SECRET=${NEWRELEASES_WEBHOOK_KEY}} \
  --zip-file fileb://lambda_function.zip
aws \
  lambda \
  create-function-url-config \
  --region "${REGION}" \
  --function-name newreleases-webhook-lambda \
  --auth-type NONE
```

Copy `FunctionUrl` in the output of last aws command.

### Update Function

```bash
# prepare function
aws \
  lambda \
  update-function-code \
  --region "${REGION}" \
  --function-name newreleases-webhook-lambda \
  --zip-file fileb://lambda_function.zip
```
