#!/usr/bin/env python3
"""
newreleases.io webhook for lambda to trigger gitlab ci pipelines
"""

import hmac
from codecs import decode
from hashlib import sha256
from json import JSONDecodeError
from json import loads as json_loads
from os import environ

from boto3 import client as boto3_client
from gitlab import Gitlab
from gitlab.exceptions import (
    GitlabAuthenticationError,
    GitlabCreateError,
    GitlabGetError,
)
from markdownify import markdownify as md


def debug(message):
    """debug"""
    if environ.get("DEBUG"):
        print(f"[DEBUG]: {message}")


def notification(message):
    """send email via sns"""
    print(message)
    try:
        sns_topic_arn = environ.get("SNS_TOPIC_ARN")
        if sns_topic_arn:
            sns = boto3_client("sns")
            sns.publish(TargetArn=sns_topic_arn, Message=message, Subject=message)
    # pylint: disable=broad-except
    except Exception as err:
        print(f"[ERROR]: {err}")


def error(project, version):
    """send error notification"""
    notification(f"newreleases web hook failed for {project} {version}")


def success(project, version):
    """send success notification"""
    notification(f"newreleases web hook successful for {project} {version}")


def gitlab_trigger_pipeline(
    gitlab_private_token: str,
    gitlab_config: dict,
    newreleases_project: str,
    newreleases_version: str,
    newreleases_note: str,
):
    """
    trigger gitlab pipeline by creating a tag
    """
    notification(f"gitlab for {newreleases_project}:{newreleases_version}")
    tag_name = newreleases_version
    has_suffix = gitlab_config.get("suffix", True)
    if has_suffix:
        tag_name = f"{newreleases_version}+1"
    gitlab = Gitlab(
        gitlab_config.get("url", "https://gitlab.com"),
        private_token=gitlab_private_token,
    )
    try:
        gitlab.auth()
    except GitlabAuthenticationError as err:
        notification(f"[ERROR]: {err}")
        return False
    try:
        project_id = gitlab_config.get("project")
        if not project_id:
            notification("[ERROR]: gitlab project not found in config")
            return False
        gitlab_project = gitlab.projects.get(project_id)
    except GitlabGetError as err:
        notification(f"[ERROR]: {err}")
        return False
    try:
        tag = gitlab_project.tags.get(tag_name)
    except GitlabGetError:
        tag = None
    if not tag:
        try:
            branch = gitlab_config.get("branch", "master")
            tag = gitlab_project.tags.create(
                {"tag_name": tag_name, "ref": branch, "message": tag_name}
            )
            if newreleases_note:
                message = md(
                    newreleases_note.get("message").replace("\\n", ""), strip=["br"]
                )
                gitlab_project.releases.create(
                    {
                        "name": tag_name,
                        "tag_name": tag_name,
                        "description": message,
                    }
                )
        except GitlabCreateError as err:
            notification(f"[ERROR]: {err}")
            return False
        trigger_token = gitlab_config.get("trigger")
        if trigger_token:
            try:
                gitlab_project.trigger_pipeline(tag_name, trigger_token)
                return True
            except GitlabCreateError as err:
                notification(f"[ERROR]: {err}")
                return False
        return True
    return True


class Request:
    """
    newreleases.io request

    - verify signature
    - verify request
    """

    def __init__(self, data):
        self.headers = data.get("headers", {})
        self.x_newreleases_signature = self.headers.get("x-newreleases-signature")
        self.x_newreleases_timestamp = self.headers.get("x-newreleases-timestamp")
        self.gitlab_private_token = self.headers.get("x-gitlab-private-token")
        self.body = data.get("body", "[]")
        self.json = json_loads(self.body)
        try:
            self.project_note = json_loads(self.json.get("project_note", "{}"))
        except JSONDecodeError:
            self.project_note = {}

    def valid_request(self):
        """
        validate request by checking for required headers
        """
        return (
            self.body
            and self.x_newreleases_signature
            and self.x_newreleases_timestamp
            and self.project_note.get("gitlab")
            and self.gitlab_private_token
        )

    def valid_signature(self):
        """
        validate signature
        see https://newreleases.io/webhooks
        """
        msg = self.x_newreleases_timestamp.encode() + b"." + self.body.encode()
        sig_computed = hmac.new(
            environ.get("NEWRELEASES_SECRET", "invalid").encode(),
            msg=msg,
            digestmod=sha256,
        ).digest()
        sig_received = decode(self.x_newreleases_signature, "hex")
        return hmac.compare_digest(sig_received, sig_computed)


# pylint: disable=unused-argument
def lambda_handler(event, context):
    """
    entrypoint
    """
    request = Request(event)
    debug(f"json = {request.json}")
    debug(f"headers = {request.headers}")
    if request.valid_request():
        if not request.valid_signature():
            notification("[ERROR]: invalid signature")
            return {"statusCode": 500}
        if not gitlab_trigger_pipeline(
            gitlab_private_token=request.gitlab_private_token,
            gitlab_config=request.project_note.get("gitlab"),
            newreleases_project=request.json.get("project"),
            newreleases_version=request.json.get("version"),
            newreleases_note=request.json.get("note"),
        ):
            return {"statusCode": 500}
        return {"statusCode": 200}
    notification("[ERROR]: invalid request")
    return {"statusCode": 500}
